import { Author } from "@/components/authors/Author";
import { useState } from "react";

export default function useSelect(
  initialValue: number,
  validationFn?: (
    arg: number,
    add?: Array<Author> | undefined
  ) => { invalid: boolean; error: string },
  additional?: Array<Author> | undefined
) {
  const [userId, setUserId] = useState(initialValue);
  const [invalid, setInvalid] = useState(false);
  const [error, setError] = useState("");

  const handleSelect = (
    author: { id: number }
  ) => {
    const { id: selectedUserId } = author;
    setUserId(selectedUserId);
    if(validationFn !== undefined) {
        let { invalid: inv, error: err } = validationFn(selectedUserId, additional);
        setInvalid(inv);
        setError(err);
      }
  };

  return {
    userId,
    invalid,
    error,
    handleSelect: handleSelect,
    setInvalid,
    setError
  };
}
