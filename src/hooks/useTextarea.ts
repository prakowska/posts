import { ChangeEvent, FocusEvent, useState } from "react";

export default function useTextArea(
  initialValue: string,
  validationFn?: (
    arg: string,
  ) => { invalid: boolean; error: string },
) {
  const [value, setValue] = useState(initialValue);
  const [invalid, setInvalid] = useState(false);
  const [error, setError] = useState("");

  const handleInputChange = (event: ChangeEvent<HTMLTextAreaElement>) => {
    setValue(event.target.value);
  };
  const handleInputBlur = (event: FocusEvent<HTMLTextAreaElement>) => {
    if(validationFn !== undefined) {
      let { invalid: inv, error: err } = validationFn(value);
      setInvalid(inv);
      setError(err);
    }
  };

  return {
    value,
    invalid,
    error,
    onChange: handleInputChange,
    onBlur: handleInputBlur,
    setInvalid,
    setError
  };
}
