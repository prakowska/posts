import { Author } from "@/components/authors/Author";
import { ChangeEvent, FocusEvent, useState } from "react";

export default function useInput(
  initialValue: string,
  validationFn?: (
    arg: string,
    add?: Array<Author> | undefined
  ) => { invalid: boolean; error: string },
  additional?: Array<Author> | undefined
) {
  const [value, setValue] = useState(initialValue);
  const [invalid, setInvalid] = useState(false);
  const [error, setError] = useState("");

  const handleInputChange = (event: ChangeEvent<HTMLInputElement>) => {
    setValue(event.target.value);
  };
  const handleInputBlur = (event: FocusEvent<HTMLInputElement>) => {
    if(validationFn !== undefined) {
      let { invalid: inv, error: err } = validationFn(value, additional);
      setInvalid(inv);
      setError(err);
    }
  };

  return {
    value,
    invalid,
    error,
    onChange: handleInputChange,
    onBlur: handleInputBlur,
    setInvalid,
    setError
  };
}
