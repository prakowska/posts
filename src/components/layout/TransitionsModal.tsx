import Backdrop from '@mui/material/Backdrop';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import Fade from '@mui/material/Fade';
import styles from './TransitionsModal.module.css';
import { ReactNode } from 'react';


interface TransitionsModalProps {
    open: boolean;
    onClose: () => void;
    children: ReactNode;
}

export default function TransitionsModal(props: TransitionsModalProps) {
  const onClose = () => props.onClose();

  return (
    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={props.open}
        onClose={onClose}
        closeAfterTransition
        slots={{ backdrop: Backdrop }}
        slotProps={{
          backdrop: {
            timeout: 500,
          },
        }}
      >
        <Fade in={props.open}>
          <Box className={styles["transition-modal-box"]}>
            {props.children}
          </Box>
        </Fade>
      </Modal>
    </div>
  );
}
