import styles from '../layout/LoadingSpinner.module.css';

export default function LoadingSpinner() {
    
  return (
    <div className={styles["loading__container"]}>
      <div className={styles["loading__spinner"]}></div>
    </div>
  );
}