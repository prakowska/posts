import React, { ReactNode } from "react";
import Navigation from "../navigation/Navigation";
import Footer from "../footer/Footer";
import styles from "../layout/Layout.module.css";

interface LayoutProps {
  children?: ReactNode;
}

export default function Layout({ children }: LayoutProps) {
  return (
    <div>
      <Navigation />
      <main className={`${styles["main"]} ${styles["block"]}`}>{children}</main>
      <Footer />
    </div>
  );
}
