import styles from './Stats.module.css';
import { Chart } from "react-google-charts";
import Breadcrumbs from "../breadcrumbs/Breadcrumbs.tsx";
import { Stats, StatsAuthor } from "./Stats.ts";
import React from 'react';
import Error from '../error/Error.tsx';


export const number = 5;
export const options = {
    title: "Statystyki autorów - Top "+number + " autorów wg liczby postów.",
};

export default function Stats({authors, authorsCount, allPosts, restOfPosts, errorMessage}: Stats) {
    const statsData = authors.map((author: StatsAuthor,index: number) => (
        <>
        <div className={styles["statistics-cell"]} key={"author"+index}>{author.name}</div> 
        <div className={styles["statistics-cell"]} key={"posts"+index}>{author.postsCount}</div>
        </>
    ))
    const header = ["Autor", "Liczba postów"];
    const chartCollect = authors.map((author: StatsAuthor,index: number) => {
        if(index + 1 <= number) return [ author.name, author.postsCount ];
        else if(index == number ) return [ "Reszta", restOfPosts];
        else return false;
    }).filter((au) => au);
    const chartData = [header , ...chartCollect];

    return (<>
        <div className={styles.statistics}>
            <Breadcrumbs name="Statystyki" />
            {authors.length > 0 &&
            <>
            <div className={styles["statistics-text"]}>
                <div className={styles["statistics-info"]}>
                    Mamy {authorsCount} {authorsCount > 1 || authorsCount==0 ? "autorów" :  "autora" }, zapoznaj się z ich statystykami!
                </div>
                <div className={styles["statistics-header"]}>Autor</div>

                <div className={styles["statistics-header"]}>Liczba postów</div>

                {statsData}

                <div className={styles["statistics-header"]}>Ogólnie</div>

                <div className={styles["statistics-header"]}>{allPosts}</div>
            </div>
            <div className={styles["statistics-charts"]}>
                <Chart
                    key="chart"
                    chartType="PieChart"
                    data={chartData}
                    options={options}
                    width={"100%"}
                    height={"400px"}
                />
            </div>
            </>}
            {errorMessage && <Error errorMessage={errorMessage}/>}
        </div>
    </>);
};