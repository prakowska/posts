import { Author } from '../authors/Author';

export interface StatsAuthor extends Omit<Author, "username" | "email">{
    postsCount: number;
}

export type Stats = {
    authors: Array<StatsAuthor>;
    authorsCount: number;
    allPosts: number;
    restOfPosts: number;
    errorMessage: string;
}