import styles from "./Post.module.css";
import Image from "next/image";
import { Post } from "./Post";
import React from "react";


export default function Post(props: Post) {
    let { userId, username, email, title, body, date } = props;
    return (
        <>
          <div className={styles["post-item"]}>
            <div className={styles["post-item-top"]}>
              <Image
                className={styles["post-item__avatar"]}
                loader={({width, src}) => `https://i.pravatar.cc/${width}${src}`}
                src={`?u=${email}`}
                priority
                width={100}
                height={100}
                alt={`Avatar of ${username}`}
              />
              <a className={styles["post-item__username"]} href={`/authors/${userId}`}>
                {username}
              </a>
            </div>
            <p className={styles["post-item__title"]}>{title}</p>
            <p className={styles["post-item__body"]}>{body.split("\n").map((item, idx) => {
                  return (
                    <React.Fragment key={idx}>
                      {item}
                      <br />
                    </React.Fragment>
                  )
                })}
            </p>
            <div className={styles["post-item__date"]}>{date}</div>
          </div>
        </>
      );
}