export interface Post {
    id: number;
    userId: number;
    username: string;
    email: string;
    title: string;
    body: string;
    date: string
};

export interface BasicPost extends Omit<Post, "id" | "username" | "email" | "date">{
}

export interface AuthorPost extends Post {
    id: number;
    userId: number;
    username: string;
    email: string;
    title: string;
    body: string;
    date: string;
    toggle: boolean
};