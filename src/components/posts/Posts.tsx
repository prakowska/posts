import styles from './Posts.module.css';
import Post from './Post.tsx';
import { Posts } from './Posts';
import { BasicPost } from './Post';
import { useState, useEffect } from 'react';
import NewPost from '../forms/NewPost.tsx';
import LoadingSpinner from '../layout/LoadingSpinner.tsx';
import Error from '../error/Error.tsx';
import Breadcrumbs from "../breadcrumbs/Breadcrumbs.tsx";


export default function Posts({ posts: fetchedPosts, errorMessage, total}: Posts) {
  const [posts, setPosts] = useState(fetchedPosts);
  const [showButton, setShowButton] = useState(true);
  const [pagination, setPagination] = useState(1);
  const [openModal, setOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(errorMessage);
  const [addedInfo, setAddedInfo] = useState("");
  const [addedPostsCount, setAddedPostsCount] = useState(0);
  const [buttonDisabled, setButtonDisabled] = useState(false);

  const loadMoreHandler = () => {
    setPagination(pagination + 1);
  };
  const handleOpenModal = () => setOpen(true);
  const handleCloseModal = () => setOpen(false);
  const handleDisable = (isDisabled: boolean) => {
    setButtonDisabled(isDisabled);
  }
  const handleAddPost = async (addedPost: BasicPost) => {
    try {
      const res = await fetch('/api/author/'+addedPost.userId);
      const { author } = await res.json();
      const postId = Math.floor(Math.random() * (100000 - 101 + 1) + 101);   //fake post id
      const timeElapsed = Date.now();
      const today = new Date(timeElapsed);
      const newPost = {
        ...addedPost,
        id: postId,
        userId: author.id,
        name: author.name,
        username: author.username,
        email: author.email,
        date: today.toLocaleString()
      };
      setPosts((posts) => [newPost, ...posts]);
      setAddedPostsCount((addedCount) => addedCount + 1);
      setOpen(false);
      setAddedInfo("Poprawnie dodano post.");
      setButtonDisabled(false);
    } catch (error) {
      setOpen(false);
      setAddedInfo("Nie udało się dodać postu- nie znaleziono autora. Spróbuj dodać innego.");
      setAddedPostsCount(0);
      setButtonDisabled(false);
    }
  };

  useEffect(() => {
    async function loadMorePosts() {
      setIsLoading(true);
      try {
        const res = await fetch(`/api/posts?page=${pagination}&limit=10`);
        const { posts: loadedPosts } = await res.json();

        if(loadedPosts?.length > 0) {
          setPosts((posts) => [...posts, ...loadedPosts]);
          setIsLoading(false);
        }
      } catch (error) {
        setIsLoading(false);
        setError("Nie udało się doładować postów. Spróbuj ponownie później");
      }
    }
    if(pagination !== 1) loadMorePosts();
  },[pagination]);
  useEffect(() => {
    if(posts?.length - addedPostsCount === total) {
      setShowButton(false);
    }
  },[posts?.length]);
  useEffect(() => {
    if (openModal === true) return;
    const disappearTimeoutId = setTimeout(() => {
      setAddedInfo("");
    }, 5000)

    return () => {
      clearTimeout(disappearTimeoutId);
    }
  }, [openModal]);


    return (<>
        <div className={styles["posts-container"]}>
          <Breadcrumbs name="Posty" />
          <button className="usual-button" type="button" onClick={handleOpenModal}>Dodaj nowy post</button>
          {addedInfo != "" && <div className={`added__info ${addedPostsCount === 0 ? " red" : ""}`}>{addedInfo}</div>}
          {openModal && <NewPost open={openModal} onClose={handleCloseModal} onAdd={handleAddPost} disabled={buttonDisabled} onDisable={handleDisable}/>}
          <div className={styles.posts}>
            {posts?.map((post) => (
                <Post
                  key={post.id}
                  id={post.id}
                  userId={post.userId}
                  username={post.username}
                  email={post.email}
                  title={post.title}
                  body={post.body}
                  date={post.date}
                />
            ))}
            {isLoading ? <LoadingSpinner /> : ""}
            {error && <Error errorMessage={error}/>}
          </div>
          {showButton && !errorMessage && <button className="usual-button" type="button" onClick={loadMoreHandler}>Załaduj więcej</button>}
        </div>
      </>
    );
}