import { Post } from './Post';

export interface Posts {
    posts: Array<Post>;
    errorMessage: string;
    total: number;
}