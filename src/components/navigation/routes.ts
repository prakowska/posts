export const routes = [
  {
    title: "Strona główna",
    href: "/",
  },
  {
    title: "Autorzy",
    href: "/authors",
  },
  {
    title: "Statystyki",
    href: "/statistics",
  },
];
