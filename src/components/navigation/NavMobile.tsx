import styles from "./Navigation.module.css";
import { useRouter } from "next/router";
import Link from "next/link";
import { routes } from "./routes";
import { useClickAway } from "react-use";
import { useRef } from "react";
import { useState } from "react";
import { AnimatePresence, motion } from "framer-motion";
import { Squash as Hamburger } from "hamburger-react";
import Image from "next/image";

export default function NavMobile() {
  const router = useRouter();
  const [isOpen, setOpen] = useState(false);
  const ref = useRef(null);

  useClickAway(ref, () => setOpen(false));

  return (
    <nav className={styles.top + " desktop-hidden"}>
      <div
        ref={ref}
        className={`${styles["top-content"]} ${styles["top-content--mobile"]}`}
      >
        <Hamburger toggled={isOpen} size={20} toggle={setOpen} />
        <AnimatePresence>
          {isOpen && (
            <motion.div
              initial={{ opacity: 0 }}
              animate={{ opacity: 1 }}
              exit={{ opacity: 0 }}
              transition={{ duration: 0.2 }}
              className={styles["top-content--expanded"]}
            >
              <ul className="grid gap-2">
                {routes.map((route, idx) => {
                  return (
                    <motion.li
                      initial={{ scale: 0, opacity: 0 }}
                      animate={{ scale: 1, opacity: 1 }}
                      transition={{
                        type: "spring",
                        stiffness: 200,
                        damping: 25,
                        delay: 0.1 + idx / 10,
                      }}
                      key={route.title}
                      className={
                        styles["navigation-item"] +
                        (router.pathname === route.href ||
                        (router.pathname.includes(route.href) &&
                          route.href != "/")
                          ? " " + styles["active"]
                          : "")
                      }
                    >
                      <Link
                        className={styles["navigation-link"]}
                        onClick={() => setOpen((prev) => !prev)}
                        href={route.href}
                      >
                        <span className="flex gap-1 text-lg">
                          {route.title}
                        </span>
                      </Link>
                    </motion.li>
                  );
                })}
              </ul>
            </motion.div>
          )}
        </AnimatePresence>
        <div className={styles["logo-top"]}>
          <Link className={styles["logo-top-link"]} href="/">
            <Image
              className={styles.logo}
              src="/forum-logo.png"
              width={100}
              height={37}
              alt="Forum logo"
            />
          </Link>
        </div>
      </div>
    </nav>
  );
}
