import { useRouter } from "next/router";
import { useClickAway } from "react-use";
import { useRef } from "react";
import { useState } from "react";
import NavMobile from "./NavMobile";
import NavDesktop from "./NavDesktop";

export default function Navigation() {

  return (
    <>
      <NavMobile />
      <NavDesktop />
    </>
  );
}
