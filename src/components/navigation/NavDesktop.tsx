import styles from "./Navigation.module.css";
import layoutStyles from "../layout/Layout.module.css";
import { useRouter } from "next/router";
import Link from "next/link";
import Image from "next/image";
import { routes } from "./routes";

export default function NavDesktop() {
  const router = useRouter();

  return (
    <nav className={styles.top + " mobile-hidden"}>
      <div
        className={`${styles["top-content"]} ${styles["top-content--desktop"]} ${layoutStyles["block"]} mobile-hidden`}
      >
        <div className={styles["logo-top"]}>
          <Link className={styles["logo-top-link"]} href="/">
            <Image
              className={styles.logo}
              src="/forum-logo.png"
              width={100}
              height={37}
              alt="Forum logo"
            />
          </Link>
        </div>
        <ul className={styles.navigation}>
          {routes.map((route) => {
            const { href, title } = route;
            return (
              <li
                key={route.title}
                className={
                  styles["navigation-item"] +
                  (router.pathname === href ||
                  (router.pathname.includes(href) && href != "/")
                    ? " " + styles["active"]
                    : "")
                }
              >
                <Link className={styles["navigation-link"]} href={href}>
                  {title}
                </Link>
              </li>
            );
          })}
        </ul>
      </div>
    </nav>
  );
}
