import styles from './Footer.module.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebookF, faLinkedin, faTwitter, faYoutube } from "@fortawesome/free-brands-svg-icons";
import layoutStyles from '../layout/Layout.module.css';

export default function Footer() {

    return (
      <footer className={styles.footer}>
        <div className={styles["footer-contents"] + " " + layoutStyles.block}>
          <div className={styles["footer-copywright"]}>
              Copywright © {(new Date().getFullYear())}.
          </div>
          <div className={styles["social-icons"]}>
            <ul className={styles["social-list"]}>
              <li className={styles["social-list__item"]}>
                <a href="https://www.facebook.com/company" target="_blank" className={styles["social-list__item--link"]} aria-label="facebook">
                  <FontAwesomeIcon icon={faFacebookF} size="xl" style={{color: "#ffffff"}} />
                </a>
              </li>
              <li className={styles["social-list__item"]}>
                <a href="https://www.linkedin.com/company/" target="_blank" className={styles["social-list__item--link"]} aria-label="linkedin">
                  <FontAwesomeIcon icon={faLinkedin} size="xl" style={{color: "#ffffff"}}/>
                </a>
              </li>
            
              <li className={styles["social-list__item"]}>
                <a href="https://www.twitter.com/company" target="_blank" className={styles["social-list__item--link"]} aria-label="twitter">
                  <FontAwesomeIcon icon={faTwitter} size="xl" style={{color: "#ffffff"}} />
                </a>
              </li>
            
              <li className={styles["social-list__item"]}>
                <a href="https://www.youtube.com/user/company" target="_blank" className={styles["social-list__item--link"]} aria-label="youtube">
                  <FontAwesomeIcon icon={faYoutube} size="xl" style={{color: "#ffffff"}} />
                </a>
              </li>
            </ul>
          </div>
        </div>
      </footer>
    );
};