import styles from "./Form.module.css";
import { ChangeEvent, FocusEvent, useRef } from "react";

interface TextareaProps {
  className: string;
  label: string;
  name: string;
  invalid: boolean;
  error: string;
  onChange: (e: ChangeEvent<HTMLTextAreaElement>) => void;
  onBlur: (e: FocusEvent<HTMLTextAreaElement>) => void;
  noRow?: boolean;
  noLabel?: boolean;
}

export default function Textarea(props: TextareaProps) {
  const {
    className,
    label,
    name,
    invalid,
    error,
    noRow,
    noLabel,
    onChange,
    onBlur,
  } = props;
  const textareaRef = useRef<HTMLTextAreaElement>(null);
  let textareaContent = 
    <>
      {!noLabel && (
        <label className={styles["form-label"]} htmlFor={label}>
          {label}
        </label>
      )}
      <textarea
        className={className}
        name={name}
        ref={textareaRef}
        onChange={onChange}
        onBlur={onBlur}
      ></textarea>
      {invalid && (
        <div className={styles["form-error"]}>
          <p>{error}</p>
        </div>
      )}
    </>;
  if (noRow) {
    return <>{textareaContent}</>;
  }
  return <div className={styles["form-row"]}>
            {textareaContent}
         </div>;
}
