import styles from "./Form.module.css";
import TransitionsModal from "../layout/TransitionsModal";
import CloseIcon from "@mui/icons-material/Close";
import {
  useState,
  useEffect,
} from "react";
import { BasicPost } from '../posts/Post';
import Input from './Input';
import Textarea from './Textarea';
import SelectItem from './Select';
import useInput from '../../hooks/useInput';
import useTextarea from '../../hooks/useTextarea';
import useSelect from '../../hooks/useSelect';
import { validateTitle, validateBody, validateUser } from '../../helpers/validations';

interface NewPostProps {
  open: boolean;
  disabled: boolean;
  onClose: () => void;
  onAdd: (inputs: BasicPost) => void;
  onDisable: (isDisabled: boolean) => void;
}

export default function NewPost(props: NewPostProps) {
  const [authors, setAuthors] = useState([]);

  const titleInput = useInput('', validateTitle);
  const bodyTextarea = useTextarea('', validateBody);
  const userSelect = useSelect(0, validateUser);
  const { open, disabled, onClose, onAdd, onDisable } = props;


  const handleSubmit = (event: React.FormEvent) => {
    event.preventDefault();
    onDisable(true);
    let {
      invalid: titleInvalid,
      error: titleError
    } =  validateTitle(titleInput.value);
    let {
      invalid: bodyInvalid,
      error: bodyError
    } = validateBody(bodyTextarea.value);
    let {
      invalid: userInvalid,
      error: userError
    } = validateUser(userSelect.userId);
    titleInput.setInvalid(titleInvalid);
    titleInput.setError(titleError);
    bodyTextarea.setInvalid(bodyInvalid);
    bodyTextarea.setError(bodyError);
    userSelect.setInvalid(userInvalid);
    userSelect.setError(userError);

    let inputs = {
      title: titleInput.value,
      body: bodyTextarea.value,
      userId: userSelect.userId
    };

    if (!(titleInvalid || bodyInvalid || userInvalid)) {
      onAdd(inputs);
    }
  };

  useEffect(() => {
    async function loadAuthors() {
      try {
        const res = await fetch("/api/authors");
        const data = await res.json();
        if (data?.authors.length > 0) setAuthors(data.authors);
      } catch (error) {
        userSelect.setInvalid(true);
        userSelect.setError("Nie można pobrać listy autorów.");
      }
    }
    loadAuthors();
  }, []);

  return (
    <TransitionsModal open={open} onClose={onClose}>
      <form onSubmit={handleSubmit}>
        <Input
          className={styles["form-input"]}
          type="text"
          label="Tytuł postu"
          name="title"
          {...titleInput}
        />
        <Textarea
          className={styles["form-textarea"]}
          label="Treść postu"
          name="body"
          {...bodyTextarea}
        />
        <SelectItem
          label="Author"
          name="select"
          authors={authors}
          {...userSelect}
        />
        <button disabled={disabled} className="usual-button">Dodaj</button>
      </form>
      <CloseIcon className={styles["form-close"]} onClick={onClose} />
    </TransitionsModal>
  );
}
