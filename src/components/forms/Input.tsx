import styles from "./Form.module.css";
import { ChangeEvent, FocusEvent, useRef } from "react";

interface InputProps {
  className: string;
  type: "text" | "email" | "tel";
  label: string;
  name: string;
  invalid: boolean;
  error: string;
  onChange: (e: ChangeEvent<HTMLInputElement>) => void;
  onBlur: (e: FocusEvent<HTMLInputElement>) => void;
  placeholder?: string;
  noRow?: boolean;
  noLabel?: boolean;
}

export default function Input(props: InputProps) {
  const {
    className,
    type,
    label,
    name,
    invalid,
    error,
    placeholder,
    noRow,
    noLabel,
    onChange,
    onBlur,
  } = props;
  const inputRef = useRef<HTMLInputElement>(null);
  let inputContent = 
    <>
      {!noLabel && (
        <label className={styles["form-label"]} htmlFor={label}>
          {label}
        </label>
      )}
      <input
        className={className}
        type={type}
        name={name}
        ref={inputRef}
        onChange={onChange}
        onBlur={onBlur}
        placeholder={placeholder}
      />
      {invalid && (
        <div className={styles["form-error"]}>
          <p>{error}</p>
        </div>
      )}
    </>;
  if (noRow) {
    return <>{inputContent}</>;
  }
  return <div className={styles["form-row"]}>
            {inputContent}
         </div>;
}
