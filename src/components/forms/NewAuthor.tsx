import styles from "./Form.module.css";
import TransitionsModal from "../layout/TransitionsModal";
import CloseIcon from "@mui/icons-material/Close";
import { useState } from "react";
import { Author } from "../authors/Author";
import Input from './Input';
import useInput from '../../hooks/useInput';
import { validateName, validateUsername, validateEmail} from '../../helpers/validations';

interface NewAuthorProps {
    open: boolean;
    authors: Array<Author> | undefined;
    disabled: boolean;
    onClose: () => void;
    onAdd: (inputs: { name: string; username: string; email: string; street?: string; number?: string; telephone?: string }) => void;
    onDisable: (isDisabled: boolean) => void;
}

export default function NewAuthor(props: NewAuthorProps) {
    const [authors, setAuthors] = useState(props.authors);

    const usernameInput = useInput('', validateUsername, authors);
    const nameInput = useInput('', validateName);
    const emailInput = useInput('', validateEmail);
    const streetInput = useInput('');
    const numberInput = useInput('');
    const telephoneInput = useInput('');

    const handleSubmit = (event: React.FormEvent) => {
      event.preventDefault();
      props.onDisable(true);
      let {
        invalid: nameInvalid,
        error: nameError
      } =  validateName(nameInput.value);
      let {
        invalid: usernameInvalid,
        error: usernameError
      } = validateUsername(usernameInput.value, authors);
      let {
        invalid: emailInvalid,
        error: emailError
      } = validateEmail(emailInput.value);
  
        nameInput.setInvalid(nameInvalid);
        nameInput.setError(nameError);
        usernameInput.setInvalid(usernameInvalid);
        usernameInput.setError(usernameError);
        emailInput.setInvalid(emailInvalid);
        emailInput.setError(emailError);
      let inputs = {
        name: nameInput.value,
        username: usernameInput.value,
        email: emailInput.value,
        street: streetInput.value,
        number: numberInput.value,
        telephone: telephoneInput.value
      };
  
      if(!(nameInvalid || usernameInvalid || emailInvalid)) {
        props.onAdd(inputs);
      }
    };
    
    
    return (
        <TransitionsModal open={props.open} onClose={props.onClose}>
          <form onSubmit={handleSubmit}>
            <Input
              className={styles["form-input"]}
              type="text"
              label="Nazwa użytkownika"
              name="username"
              {...usernameInput}
            />
            <Input
              className={styles["form-input"]}
              type="text"
              label="Imię i nazwisko"
              name="name"
              {...nameInput}
            />
            <Input
              className={styles["form-input"]}
              type="email"
              label="E-mail"
              name="email"
              {...emailInput}
            />
            <div className={`${styles["form-row"]} ${styles["form-horizontal"]}`}>
              <Input 
                className={`${styles["form-input"]} ${styles["form-wider"]}`}
                type="text"
                label="Adres"
                name="street"
                placeholder="Ulica"
                noRow={true}
                {...streetInput}
              />
              <Input 
                className={`${styles["form-input"]} ${styles["form-narrower"]}`}
                type="text"
                label=""
                name="number"
                placeholder="Numer"
                noRow={true}
                noLabel={true}
                {...numberInput}
              />
            </div>
            <Input 
                className={`${styles["form-input"]}`}
                type="tel"
                label="Telefon"
                name="telephone"
                placeholder="+48 123-456-789"
                {...telephoneInput}
            />
            <button className="usual-button" disabled={props.disabled}>Dodaj</button>
          </form>
          <CloseIcon className={styles["form-close"]} onClick={props.onClose} />
        </TransitionsModal>
      );
}