import styles from "./Form.module.css";
import { Author } from '../authors/Author';
import Box from "@mui/material/Box";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import Image from "next/image";

interface SelectProps {
  label: string;
  name: string;
  userId: number;
  invalid: boolean;
  error: string;
  authors: Array<Author>;
  handleSelect: (author: Author | { id: number }) => void;
  noRow?: boolean;
  noLabel?: boolean;
}

export default function SelectItem(props: SelectProps) {
  const {
    label,
    name,
    userId,
    invalid,
    error,
    authors,
    noRow,
    noLabel,
    handleSelect
  } = props;
  const ITEM_HEIGHT = 48;
  const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * 4.5,
      },
    },
  };
  let selectContent = 
    <>
      {!noLabel && (
        <label className={styles["form-label"]} htmlFor={label}>
          {label}
        </label>
      )}
      <Box sx={{ minWidth: 120 }}>
            <FormControl fullWidth>
              <Select
                MenuProps={MenuProps}
                name={name}
                value={userId}
              >
                <MenuItem
                  value={userId}
                  key={0}
                  onClick={() => handleSelect({ id: 0 })}
                >
                  <div className={styles["form-option-item"]}>
                    <span className={styles["form-option-placeholder"]}>
                      Wybierz autora
                    </span>
                  </div>
                </MenuItem>
                {authors.map(
                  (author: Author) => (
                    <MenuItem
                      value={author.id}
                      key={author.id}
                      onClick={() => handleSelect(author)}
                    >
                      <div className={styles["form-option-item"]}>
                        <Image
                          className={styles["form-option-avatar"]}
                          loader={({ width, src }) =>
                            `https://i.pravatar.cc/${width}${src}`
                          }
                          src={`?u=${author.email}`}
                          priority
                          width={50}
                          height={50}
                          alt={`Avatar of ${author.username}`}
                        />
                        <span className={styles["form-option-username"]}>
                          {author.username}
                        </span>
                      </div>
                    </MenuItem>
                  )
                )}
              </Select>
            </FormControl>
          </Box>
      {invalid && (
        <div className={styles["form-error"]}>
          <p>{error}</p>
        </div>
      )}
    </>;
  if (noRow) {
    return <>{selectContent}</>;
  }
  return <div className={styles["form-row"]}>
            {selectContent}
         </div>;
}
