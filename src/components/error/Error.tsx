import styles from './Error.module.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTriangleExclamation } from "@fortawesome/free-solid-svg-icons";
import { ErrorMessage } from './Error';

export default function Error({ errorMessage} : ErrorMessage) {
    return (
      <div className={styles.error}>
            <FontAwesomeIcon icon={faTriangleExclamation} fade />
            {errorMessage}
      </div>
    );
  };
