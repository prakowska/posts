import Head from "next/head";

interface HeadTagProps {
  title: string;
  description: string;
}

export default function HeadTag(props: HeadTagProps) {
  const { title, description } = props;

  return (
    <Head>
      <title>{title}</title>
      <meta name="description" content={description} />
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1.0, maximum-scale=5"
      />
      <link rel="icon" href="/favicon.ico" />
    </Head>
  );
}
