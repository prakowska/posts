interface BreadcrumbsProps {
  name: string;
  secondname?: string;
}

export default function Breadcrumbs(props: BreadcrumbsProps) {
  const { name, secondname } = props;

  return (
    <div className="breadcrumbs">
        <h1>{name} &gt; {secondname}</h1>
    </div>
  );
}
