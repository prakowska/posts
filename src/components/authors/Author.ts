import { AuthorPost, Post } from '../posts/Post';

export interface Author {
    id: number;
    name: string;
    username: string;
    email: string;
    address?: {
        street?: string;
        suite?: string;
        city?: string;
        zipcode?: string;
        geo?: {
            lat?: string;
            lng?: string;
        }
    };
    phone?: string;
    website?: string;
    company?: {
        name?: string;
        catchPhrase?: string;
        bs?: string;
    };
    image?: boolean;
    hideLink?: boolean;
    posts?: Array<Post>;
    postsCount?: number;
};
export interface BasicAuthor {
    name: string;
    username: string;
    email: string;
    street?: string;
    number?: string;
    telephone?: string;
};

export interface AuthorData {
    author: Author | {};
    status: number;
    errorMessage : string;
}