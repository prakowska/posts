import styles from "../authors/Author.module.css";
import React, { useState, SetStateAction } from "react";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Box from "@mui/material/Box";
import CustomTabPanel from "../layout/CustomTabPanel";
import Image from 'next/image';
import Breadcrumbs from "../breadcrumbs/Breadcrumbs.tsx";
import { Author, AuthorData } from "./Author";
import { AuthorPost, Post }  from '../posts/Post';
import Error from '../error/Error.tsx';


function tabsProps(index: number) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}
export default function AuthorComponent(props: AuthorData) {
  let { author = {}, errorMessage } = props;
  let { id, name, username, email, address, phone, website, company, posts } = author as Author;

  let mapUrl =
    "https://maps.google.com/maps?q=" +
    address?.geo?.lat +
    "%20" +
    address?.geo?.lng +
    "&t=&z=13&ie=UTF8&iwloc=&output=embed";
  const [tabValue, setTabValue] = useState(0);
  const authorPosts = posts?.map((post: AuthorPost | Post, index: number) => {
    return index == 0? {
      ...post,
      toggle: true
    }: {
      ...post,
      toggle: false
    }
  });
  const [togglePosts, setTogglePosts] = useState(authorPosts);

  const handleChange = (event: React.SyntheticEvent<Element, Event>, newValue: React.SetStateAction<number>) => {
    setTabValue(newValue);
  };
  const handleToggle = (name: string, index: number) => {
    let tPosts = togglePosts?.map((post: AuthorPost, ind: number) => {
      return index == ind? {
      ...post,
      [name]: !post.toggle
    }: post });
    setTogglePosts(tPosts);
  };


  return (
    <>
      {id && 
      <div className={styles["author"]}>
        <Breadcrumbs name="Autorzy" secondname={name}/>
        <div className={styles["author-top"]}>
          <Image
            className={styles["author__avatar"]}
            loader={({width, src}) => `https://i.pravatar.cc/${width}${src}`}
            src={`?u=${email}`}
            priority
            width={250}
            height={250}
            alt={`Avatar of ${username}`}
          />
          <p className={styles["author__name"]}>{name}</p>
          <p className={styles["author__type"]}>Autor</p>
        </div>
        <Box sx={{ width: "100%" }}>
          <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
            <Tabs
              className={styles["author__tabs"]}
              value={tabValue}
              onChange={handleChange}
              aria-label="Autor zakładki"
              TabIndicatorProps={{
                style: {
                  backgroundColor: "#77b539",
                },
              }}
            >
              <Tab
                className={styles["author__tab"]}
                label="O autorze"
                {...tabsProps(0)}
              />
              <Tab
                className={styles["author__tab"]}
                label="Posty"
                {...tabsProps(1)}
              />
            </Tabs>
          </Box>
          <CustomTabPanel
            value={tabValue}
            index={0}
            className={styles["author-tab"]}
          >
            <div className={styles["author-details"]}>
              <div className={styles["author-detail"]}>
                <div className={styles["author-detail-label"]}>Nick:</div>{" "}
                {username}
              </div>
              <div className={styles["author-detail"]}>
                <div className={styles["author-detail-label"]}>E-mail:</div>{" "}
                {email}
              </div>
              <div className={styles["author-detail"]}>
                <div className={styles["author-detail-label"]}>Adres:</div>{" "}
                {address?.street} {address?.suite} {address?.city}{" "}
                {address?.zipcode}
              </div>
              <div className={styles["author-detail"]}>
                <div className={styles["author-detail-label"]}>Telefon:</div>{" "}
                {phone}
              </div>
              <div className={styles["author-detail"]}>
                <div className={styles["author-detail-label"]}>Strona:</div>{" "}
                <a href={`https://${website}`}>{website}</a>
              </div>
              <div className={styles["author-detail"]}>
                <div className={styles["author-detail-label"]}>Firma:</div>{" "}
                {company?.name}
              </div>
              <div className={styles["author-detail"]}>
                <div className={styles["author-detail-label"]}>Slogan firmy:</div>{" "}
                {company?.catchPhrase}
              </div>
              <div className={styles["author-detail"]}>
                <div className={styles["author-detail-label"]}>Usługi biznesowe:</div>{" "}
                {company?.bs}
              </div>
            </div>
            <div className={styles["author-map"]}>
              <div className={styles["author-inner"]}>
                <iframe
                  id="gmap_canvas"
                  src={mapUrl}
                ></iframe>
              </div>
            </div>
          </CustomTabPanel>
          <CustomTabPanel value={tabValue} index={1}>
            {togglePosts?.map((post: AuthorPost, index: number) => {

              return (
              <div key={post.id} className={post.toggle ? `${styles["author-post"]} ${styles["opened"]}` : `${styles["author-post"]} ${styles["closed"]}`}>
                <p className={styles["author-post__title"]} onClick={(e) => handleToggle("toggle", index)}>
                  {post.title}
                  <span className={`${styles["author-post__toggle"]}  ${styles["opened"]}`}>
                    <svg fill="#000000" height="800px" width="800px" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" 
                      viewBox="0 0 330 330" xmlSpace="preserve">
                    <path id="XMLID_225_" d="M325.607,79.393c-5.857-5.857-15.355-5.858-21.213,0.001l-139.39,139.393L25.607,79.393
                      c-5.857-5.857-15.355-5.858-21.213,0.001c-5.858,5.858-5.858,15.355,0,21.213l150.004,150c2.813,2.813,6.628,4.393,10.606,4.393
                      s7.794-1.581,10.606-4.394l149.996-150C331.465,94.749,331.465,85.251,325.607,79.393z"/>
                    </svg>
                  </span>
                  <span className={`${styles["author-post__toggle"]}  ${styles["closed"]}`}>
                    <svg fill="#000000" height="800px" width="800px" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" 
                      viewBox="0 0 330 330" xmlSpace="preserve">
                    <path id="XMLID_222_" d="M250.606,154.389l-150-149.996c-5.857-5.858-15.355-5.858-21.213,0.001
                      c-5.857,5.858-5.857,15.355,0.001,21.213l139.393,139.39L79.393,304.394c-5.857,5.858-5.857,15.355,0.001,21.213
                      C82.322,328.536,86.161,330,90,330s7.678-1.464,10.607-4.394l149.999-150.004c2.814-2.813,4.394-6.628,4.394-10.606
                      C255,161.018,253.42,157.202,250.606,154.389z"/>
                    </svg>
                  </span>
                </p>
                <p
                  className={styles["author-post__body"]}
                >
                  {post?.body?.split("\n")?.map((item, idx) => {
                  return (
                    <React.Fragment key={idx}>
                      {item}
                      <br />
                    </React.Fragment>
                  )
                })}
                 {/* Pretending that post have a date*/}
                  <span className={styles["author-post__date"]}>{post.date}</span>
                </p>
              </div>
              )
            })}
          </CustomTabPanel>
        </Box>
      </div>}
      {errorMessage && <Error errorMessage={errorMessage}/>}
    </>
  );
};