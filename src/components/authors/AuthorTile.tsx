import { Author } from "./Author";
import styles from "./AuthorTile.module.css";
import Image from "next/image";

export default function AuthorTile(props: Author) {
  let {
    id,
    name,
    username,
    image = true,  //for jsonplaceholder users use avatar as default, newly added users will have this to false
    email,
    postsCount,
    hideLink = false,  //hiding link is used for newly added users
  } = props;
  return (
    <>
      <div className={styles["author-item"]}>
        <div className={styles["author-item-left"]}>
          {image && (
            <Image
              className={styles["author-item__avatar"]}
              loader={({ width, src }) =>
                `https://i.pravatar.cc/${width}${src}`
              }
              src={`?u=${email}`}
              priority
              width={100}
              height={100}
              alt={`Avatar of ${username}`}
            />
          )}
          {!image && (
            <Image
              className={styles["author-item__avatar"]}
              src="/default-avatar.jpg"
              width={100}
              height={100}
              alt={`Avatar of ${username}`}
            />
          )}
        </div>
        <div className={styles["author-item-right"]}>
          <p className={styles["author-item__username"]}>{username}</p>
          <p className={styles["author-item__name"]}>{name}</p>
          <p className={styles["author-item__email"]}>{email}</p>
          <div className={styles["author-bottom"]}>
            <p className={styles["author-item__posts"]}>
              {postsCount}{" "}
              {postsCount! > 4 || postsCount == 0
                ? "postów"
                : postsCount == 1
                ? "post"
                : "posty"}
            </p>
            {!hideLink && (
              <a
                className={styles["author-item__link"]}
                href={`/authors/${id}`}
              >
                Dowiedz się więcej &gt;
              </a>
            )}
          </div>
        </div>
      </div>
    </>
  );
}
