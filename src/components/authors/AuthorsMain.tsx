import styles from '../authors/Authors.module.css';
import { useState, useEffect } from "react";
import AuthorTile from '../authors/AuthorTile';
import NewAuthor from '../forms/NewAuthor';
import Breadcrumbs from "../breadcrumbs/Breadcrumbs.tsx";
import { Authors } from '../authors/Authors';
import { BasicAuthor, Author } from '../authors/Author';
import Error from '../error/Error.tsx';

export default function AuthorsMain({ authors: fetchedAuthors, errorMessage}: Authors) {
  const [authors, setAuthors] = useState(fetchedAuthors);
  const [openModal, setOpen] = useState(false);
  const [addedInfo, setAddedInfo] = useState("");
  const [buttonDisabled, setButtonDisabled] = useState(false);

  const handleOpenModal = () => setOpen(true);
  const handleCloseModal = () => setOpen(false);
  const handleDisable = (isDisabled: boolean) => {
      setButtonDisabled(isDisabled);
  }
  const handleAddAuthor = async (addedAuthor: BasicAuthor) => {
    const authorId = Math.floor(Math.random() * (100000 - 101 + 1) + 101);  
    const newAuthor = {
      ...addedAuthor,
      id: authorId,
      image: false,
      postsCount: 0,
      hideLink: true
    };
    setAuthors((authors) => [newAuthor, ...authors]);
    setOpen(false);
    setAddedInfo("Poprawnie dodano autora.");
    setButtonDisabled(false);
  };

  useEffect(() => {
    if (openModal === true) return;
    const disappearTimeoutId = setTimeout(() => {
      setAddedInfo("");
    }, 5000)

    return () => {
      clearTimeout(disappearTimeoutId);
    }
  }, [openModal]);


    return (<>
        <div className={styles["authors"]}>
          <Breadcrumbs name="Autorzy" />
          <button className={`${styles["authors-button"]} usual-button`} type="button" onClick={handleOpenModal}>Dodaj autora</button>
          {addedInfo != "" && <div className="added__info">{addedInfo}</div>}
          {openModal && <NewAuthor open={openModal} onClose={handleCloseModal} authors={authors} onAdd={handleAddAuthor} disabled={buttonDisabled} onDisable={handleDisable}/>}
          {authors.map((author: Author) => (
              <AuthorTile
                key={author.id}
                id={author.id}
                name={author.name}
                username={author.username}
                image={author.image}
                email={author.email}
                postsCount={author.postsCount}
                hideLink={author.hideLink? true: false}
              />
          ))}
          {errorMessage && <Error errorMessage={errorMessage}/>}
        </div>
      </>
    );
}