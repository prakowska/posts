import { Author } from './Author';

export interface Authors {
    authors: Array<Author>;
    errorMessage: string;
}