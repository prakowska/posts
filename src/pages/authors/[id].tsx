import { useRouter } from "next/router";
import HeadTag from "../../components/head/HeadTag";
import AuthorComponent from "../../components/authors/AuthorComponent";
import { AuthorData } from "../../components/authors/Author";

export default function Author(props: AuthorData) {
  const router = useRouter();
  const id: number = Number(router.query?.id);
  
  return (
    <>
      <HeadTag title="Autor" description="Szczegóły o autorze" />
      <div className="Author">
        <AuthorComponent key={id} {...props} />
      </div>
    </>
  );
}

export const getServerSideProps = async (context: { params: { id: number; }; }) => {
  const id = context.params.id;

  const res = await fetch(`http://localhost:3000/api/author/${id}`);
  const data = await res.json();
  return {
    props: data,
  };
};
