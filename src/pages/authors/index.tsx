import HeadTag from "../../components/head/HeadTag";
import AuthorsMain from "../../components/authors/AuthorsMain";
import { Authors } from "../../components/authors/Authors";

export default function Authors(props: Authors) {
  return (
    <>
      <HeadTag title="Autorzy" description="Przegląd autorów" />
      <div className="Authors">
        <AuthorsMain authors={props.authors} errorMessage={props.errorMessage} />
      </div>
    </>
  );
}

export const getServerSideProps = async () => {
  const res = await fetch('http://localhost:3000/api/authors');
  const data = await res.json();
  
  return {
    props: data
  }
};
