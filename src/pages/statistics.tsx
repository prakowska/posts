import HeadTag from "../components/head/HeadTag";
import { Stats as StatsType } from  '../components/statistics/Stats';
import Stats from  '../components/statistics/Stats.tsx';

export default function Statistics(props: StatsType) {
  return (
    <>
      <HeadTag title="Statystyki" description="Statystyki najpopularniejszych autorów" />
      <div className="Statistics">
        <Stats key="stats" {...props} />
      </div>
    </>
  );
}

export const getServerSideProps = async () => {
  const res = await fetch('http://localhost:3000/api/statistics/5');
  const data = await res.json();
  
  return {
    props: data
  }
};
