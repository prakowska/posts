import HeadTag from '../components/head/HeadTag';
import Posts from  '../components/posts/Posts.tsx';
import { Post } from '../components/posts/Post';

export type PostsData = {
  posts: Array<Post>;
  errorMessage: string;
  total: number;
}

export default function HomePage(props: PostsData) {
  return (
    <>
      <HeadTag title="Strona z postami" description="Posty na forum." />
      <div className="Homepage">
        <Posts key="posts" {...props} />
      </div>
    </>
  );
}

export const getServerSideProps = async () => {
  const res = await fetch('http://localhost:3000/api/posts?limit=10&page=1');
  const data = await res.json();
  
  return {
    props: data
  }
}