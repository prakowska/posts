// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import axios, { AxiosResponse } from 'axios';
import { Author } from "../../../components/authors/Author.ts";
import { AuthorData } from "../../../components/authors/Author.ts";
import { Post } from "../../../components/posts/Post.ts";
import { generateDate } from '../../../helpers/date.ts';

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<AuthorData>
) {
    try {
      const id = req.query.id? Number(req.query.id) : 0;
      const response: AxiosResponse<Array <Author>> = await axios.get(`https://jsonplaceholder.typicode.com/users?id=${id}`);
      if(response.statusText !=='OK') {
        throw new Error("Nie udało się pobrać danych autora.");
      }
      else{
        if(response?.data.length === 0){
          res.status(404).json({
            author: {},
            status: 404,
            errorMessage: "Podany autor nie istnieje."
          });
        }
        else{
          let postData: AxiosResponse<Array <Post>> = await axios.get(`https://jsonplaceholder.typicode.com/posts?userId=${id}`);
          if(response.statusText !=='OK') {
            throw new Error("Nie udało się pobrać postów autora.");
          }
          {
            let posts = postData?.data?.map((post: Post) => ({...post, date: generateDate(post.id)}));
            let author = response?.data[0];
  
            let userData = {
                ...author,
                posts: posts
            }
            res.status(200).json({
              author: userData, 
              status: 200,
              errorMessage: ""
            });
          }
        }
      }
    } catch (error) {
        res.status(500).json({
          author: {},
          status: 500,
          errorMessage: "Nie udało się pobrać danych autora. Spróbuj ponownie później.",
        });
    }
}