// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import axios, { AxiosResponse } from 'axios';
import { Post } from "../../../components/posts/Post.ts";
import { Stats, StatsAuthor } from '../../..//components/statistics/Stats.ts';
import Qs from "qs";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Stats>
) {
    try {
      const number = req.query.number? Number(req.query.number) : 5;
      const response: AxiosResponse<Array <StatsAuthor>> = await axios.get('https://jsonplaceholder.typicode.com/users?_sort=name');
      if(response.statusText !=='OK') {
        throw new Error("Nie udało się pobrać autorów do statystyk.");
      }
      else {
        const usersIds = response.data?.map((author) => author.id);
        const users = new Set(usersIds);
        let userParams = { userId: users };
        let params = Qs.stringify(userParams, { arrayFormat: "repeat" });
        let postData: AxiosResponse<Array <Post>> = await axios.get(
          `https://jsonplaceholder.typicode.com/posts?` + params
        );
        if(postData.statusText !=='OK') {
          throw new Error("Nie udało się pobrać postów.");
        }
        else {
          let userData: Array<StatsAuthor> = response.data?.map((user: StatsAuthor, index: number) => ({
            id: user.id,
            name: user.name,
            postsCount: postData?.data.filter((post: Post) => user.id === post.userId).length
          }));
          //Alternative more realistic data
          // let userData = [
          //   { id: 5, name: 'Chelsey Dietrich', postsCount: 7 },
          //   { id: 10, name: 'Clementina DuBuque', postsCount: 20 },
          //   { id: 3, name: 'Clementine Bauch', postsCount: 11 },
          //   { id: 2, name: 'Ervin Howell', postsCount: 39 },
          //   { id: 9, name: 'Glenna Reichert', postsCount: 2 },
          //   { id: 7, name: 'Kurtis Weissnat', postsCount: 10 },
          //   { id: 1, name: 'Leanne Graham', postsCount: 100 },
          //   { id: 6, name: 'Mrs. Dennis Schulist', postsCount: 5 },
          //   { id: 8, name: 'Nicholas Runolfsdottir V', postsCount: 17 },
          //   { id: 4, name: 'Patricia Lebsack', postsCount: 0 }
          // ];
          const allPosts = userData.reduce((accumulator, user) => {
            return accumulator + user.postsCount;
          }, 0);
          userData = userData.sort((a, b) => (b.postsCount - a.postsCount));
          let restData = userData.filter((user, index) => (index >= number && user));
          const restOfPosts = restData.reduce((accumulator, user) => {
            return accumulator + user.postsCount;
          }, 0);

          res.status(200).json({
            authors: userData,
            authorsCount: userData?.length,
            allPosts: allPosts,
            restOfPosts: restOfPosts,
            errorMessage: ""
          });
        }
      }
    } catch (error) {
      res.status(500).json({
        authors: [],
        authorsCount: 0,
        allPosts: 0,
        restOfPosts: 0,
        errorMessage: "Nie udało się pobrać statystyk. Spróbuj ponownie później."
      });
    }
}