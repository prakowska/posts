// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import axios, { AxiosResponse } from "axios";
import { Authors } from "../../components/authors/Authors.ts";
import { Author } from "../../components/authors/Author.ts";
import { Post } from "../../components/posts/Post.ts";
import Qs from "qs";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Authors>
) {
  try {
    const response: AxiosResponse<Array<Author>> = await axios.get(
      `https://jsonplaceholder.typicode.com/users?_sort=name`
    );
    if(response.statusText !=='OK') {
      throw new Error("Nie udało się pobrać autorów.");
    }
    else {
      const usersIds = response.data?.map((author) => author.id);
      const users = new Set(usersIds);
      let userParams = { userId: users };
      let params = Qs.stringify(userParams, { arrayFormat: "repeat" });
      let postData: AxiosResponse<Array <Post>> = await axios.get(
        `https://jsonplaceholder.typicode.com/posts?` + params
      );
      if(postData.statusText !=='OK') {
        throw new Error("Nie udało się pobrać postów autorów.");
      }
      else {
        let userData: Array<Author> = response.data?.map((user: Author) => ({
          id: user.id,
          name: user.name,
          username: user.username,
          email: user.email,
          postsCount: postData?.data.filter((post: Post) => user.id === post.userId).length
        }));
        res.status(200).json({
          authors: userData,
          errorMessage: ""
        });
      }
    }
  } catch (error) {
    res.status(500).json({
      authors: [],
      errorMessage: "Nie udało się pobrać autorów. Spróbuj ponownie później.",
    });
  }

}
