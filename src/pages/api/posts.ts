// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next';
import axios, { AxiosResponse } from 'axios';
import { Post } from "../../components/posts/Post.ts";
import { Posts } from "../../components/posts/Posts.ts";
import { Author } from "../../components/authors/Author.ts";
import { generateDate } from '../../helpers/date.ts';
import Qs from 'qs';

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Posts>
) {
    const page = req.query.page? Number(req.query.page) : 1;
    const limit = req.query.limit? Number(req.query.limit) : 10;
    try {
      const response: AxiosResponse<Array <Post>> = await axios.get(`https://jsonplaceholder.typicode.com/posts?_sort=title&_page=${page}&_limit=${limit}`);
      if(response.statusText !=='OK') {
        throw new Error("Nie udało się pobrać postów.");
      }
      else {
        const usersIds = response.data?.map(post => (post.userId));
        const users = new Set(usersIds);
        let userParams = { id: users };
        let params = Qs.stringify(userParams, {arrayFormat: 'repeat'});
        let userData: AxiosResponse<Array <Author>> = await axios.get(`https://jsonplaceholder.typicode.com/users?`+params);
        if(userData.statusText !=='OK') {
          throw new Error("Nie udało się pobrać danych autorów.");
        }
        else {
          let postData: Array<Post> = response.data?.map((post: Post) => ({...userData?.data.find(user => user.id === post.userId),...post, date: generateDate(post.id)}));
  
          res.status(200).json({
            posts: postData,
            errorMessage: "",
            total: parseInt(response.headers['x-total-count'])
          });
        }
      }
    } catch (error) {
      res.status(500).json({
        posts: [],
        errorMessage: "Nie udało się pobrać postów. Spróbuj ponownie później.",
        total: 0
      });
    }
}
