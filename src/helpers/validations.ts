import { Author } from "@/components/authors/Author";

export const validateName = (name: string) => {
    if (name.trim().length === 0) {
      return { 
        invalid: true,
        error: "Imię i nazwisko są wymagane"
      };
    } else if (name.trim().length < 3) {
      return { 
        invalid: true,
        error: "Imię i nazwisko nie mogą mieć mniej niż 3 znaki."
      }
    }  else {
      return { 
        invalid: false,
        error: ""
      }
    }
};
export const validateUsername = (username: string, authors: Array<Author> | undefined) => {
    let usernameRegex = /^[a-z0-9_.]+$/i;
    let validUsername = username?.trim()?.match(usernameRegex);
    if (username.trim().length === 0) {
      return { 
        invalid: true,
        error: "Nazwa użytkownika jest wymagana."
      }
    } else if (username.trim().length < 2 || username.trim().length > 20) {
      return { 
        invalid: true,
        error: "Dozwolona nazwa użytkownika wynosi od 2 do 20 znaków."
      }
    } else if (!validUsername) {
      return { 
        invalid: true,
        error: "Niedozwolony znak. Nazwa użytkownika może składać się tylko z małych, dużych liter, cyfr, kropki i podkreślnika."
      }
    } else {
      const searchUsername = authors?.find((author: Author) => author.username == username.trim());
      if(searchUsername) {
        return { 
          invalid: true,
          error: "Podany użytkownik już istnieje. Wybierz inną nazwę."
        }
      }
      else {
        return { 
          invalid: false,
          error: ""
        }
      }
    }
};
export const validateEmail = (email: string) => {
    let emailRegex = /^([-!#-'*+/-9=?A-Z^-~]+(\.[-!#-'*+/-9=?A-Z^-~]+)*|"([]!#-[^-~ \t]|(\\[\t -~]))+")@[0-9A-Za-z]([0-9A-Za-z-]{0,61}[0-9A-Za-z])?(\.[0-9A-Za-z]([0-9A-Za-z-]{0,61}[0-9A-Za-z])?)+$/;
    let validEmail = email?.trim()?.match(emailRegex);
    if (email.trim().length === 0) {
      return { 
        invalid: true,
        error: "E-mail jest wymagany."
      }
    }
    else if(!validEmail){
      return { 
        invalid: true,
        error: "Niepoprawny e-mail."
      }
    }
    else {
      return { 
        invalid: false,
        error: ""
      }
    }
};
export const validateTitle = (title: string) => {
  if (title.trim().length === 0) {
    return {
      invalid: true,
      error: "Tytuł jest wymagany",
    };
  } else if (title.trim().length < 5) {
    return {
      invalid: true,
      error: "Tytuł musi mieć przynajmniej 5 znaków",
    };
  } else if (title.trim().length > 50) {
    return {
      invalid: true,
      error: "Tytuł nie może przekraczać 50 znaków",
    };
  } else {
    return {
      invalid: false,
      error: "",
    };
  }
};
export const validateBody = (body: string) => {
  if (body.trim().length === 0) {
    return {
      invalid: true,
      error: "Treść jest wymagana",
    };
  } else if (body.trim().length < 5 || body.trim().length > 500) {
    return {
      invalid: true,
      error: "Treść musi mieć pomiędzy 5 a 500 znaków",
    };
  } else {
    return {
      invalid: false,
      error: "",
    };
  }
};
export const validateUser = (userId: number) => {
  return userId == 0
    ? {
        invalid: true,
        error: "Autor jest wymagany",
      }
    : {
        invalid: false,
        error: "",
      };
};