export function generateDate(postId: number){
  let initialDate = '01-01-2024';
  let initialTmp = new Date(initialDate).getTime();
  let generateNum = initialTmp - 50000000 * postId;  //generate fake date by post id
  let date = new Date(generateNum);
  return date.toLocaleString();
}